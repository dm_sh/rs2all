BOARD_PATH := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))

C_DEFS += -DUSE_HAL_DRIVER
C_DEFS += -DSTM32F030x6

C_INCLUDE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Inc
C_INCLUDE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Inc/Legacy
C_INCLUDE += $(BOARD_PATH)/Drivers/CMSIS/Device/ST/STM32F0xx/Include
C_INCLUDE += $(BOARD_PATH)/Drivers/CMSIS/Include
C_INCLUDE += $(BOARD_PATH)/Core/Inc
C_INCLUDE += $(BOARD_PATH)/src

C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_adc.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_adc_ex.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_cortex.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_dma.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_flash.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_flash_ex.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_gpio.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_iwdg.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_i2c.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_i2c_ex.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_pwr.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_pwr_ex.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_rcc.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_rcc_ex.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_tim.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_tim_ex.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_uart.c
C_SOURCE += $(BOARD_PATH)/Drivers/STM32F0xx_HAL_Driver/Src/stm32f0xx_hal_uart_ex.c
C_SOURCE += $(BOARD_PATH)/Core/Src/system_stm32f0xx.c
C_SOURCE += $(BOARD_PATH)/Core/Src/sysmem.c
C_SOURCE += $(BOARD_PATH)/Core/Src/syscalls.c
C_SOURCE += $(BOARD_PATH)/src/clock.c
C_SOURCE += $(BOARD_PATH)/src/systick.c
C_SOURCE += $(BOARD_PATH)/src/led.c
C_SOURCE += $(BOARD_PATH)/src/i2c.c

ASM_SOURCES = $(BOARD_PATH)/Core/Startup/startup_stm32f030k6tx.s

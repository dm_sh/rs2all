#include "i2c.h"

static I2C_HandleTypeDef _i2c1;

static void _i2c_gpio_init();

bool i2c_init() {
  _i2c_gpio_init();

  _i2c1.Instance = I2C1;
	_i2c1.Init.Timing = 0x00707CBB;
	_i2c1.Init.OwnAddress1 = 0;
	_i2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	_i2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	_i2c1.Init.OwnAddress2 = 0;
	_i2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	_i2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	_i2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;

	if (HAL_I2C_Init(&_i2c1) != HAL_OK) {
		return false;
	}

	if (HAL_I2CEx_ConfigAnalogFilter(&_i2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK) {
		return false;
	}

	if (HAL_I2CEx_ConfigDigitalFilter(&_i2c1, 0) != HAL_OK) {
		return false;
	}

	return true;
}

bool i2c_read(uint8_t address, uint8_t reg, uint8_t *data, uint16_t data_len) {
	uint8_t tx_buffer[SENSOR_I2C_BUFFER_SIZE] = {0};

	tx_buffer[0] = reg;

	if (HAL_I2C_Master_Transmit(&_i2c1, (address << 1), tx_buffer, 1, 100) != HAL_OK) {
		return false;
	}

	if (HAL_I2C_Master_Receive(&_i2c1, (address << 1), data, data_len, 100) != HAL_OK) {
		return false;
	}

	return true;
}

bool i2c_write(uint8_t address, uint8_t reg, uint8_t *data, uint16_t data_len) {
	uint8_t tx_buffer[SENSOR_I2C_BUFFER_SIZE];

	tx_buffer[0] = reg;

	if (data_len >= SENSOR_I2C_BUFFER_SIZE) {
		return false;
	}

	memcpy(&tx_buffer[1], data, data_len);

	if (HAL_I2C_Master_Transmit(&_i2c1, (address << 1), tx_buffer, (data_len + 1), 100) != HAL_OK) {
		return false;
	}

	return true;
}

static void _i2c_gpio_init() {
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	GPIO_InitStruct.Pin       = SENSORS_I2C_SCL|SENSORS_I2C_SDA;
	GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
	GPIO_InitStruct.Pull      = GPIO_PULLUP;
	GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = SENSORS_I2C_AF;
	HAL_GPIO_Init(SENSORS_I2C_PORT, &GPIO_InitStruct);
}
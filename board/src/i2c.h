#ifndef I2C_H_
#define I2C_H_

#include "board.h"

bool i2c_init();
bool i2c_write(uint8_t address, uint8_t reg, uint8_t *data, uint16_t data_len);
bool i2c_read(uint8_t address, uint8_t reg, uint8_t *data, uint16_t data_len);

#endif

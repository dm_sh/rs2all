#ifndef LED_H_
#define LED_H_

#include "stm32f0xx_hal.h"

void led_init();
void led_toggle();

#endif

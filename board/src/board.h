#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "stm32f0xx_hal.h"
#include "stm32f0xx_it.h"

#define SENSORS_I2C_SCL 		    	GPIO_PIN_9
#define SENSORS_I2C_SDA				    GPIO_PIN_10
#define SENSORS_I2C_PORT			    GPIOA
#define SENSORS_I2C_AF				    GPIO_AF4_I2C1
#define SENSOR_I2C_BUFFER_SIZE		0x20

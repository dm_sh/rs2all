#include "stm32f0xx_hal.h"
#include "stm32f0xx_it.h"

void HAL_MspInit(void)
{
  __HAL_RCC_SYSCFG_CLK_ENABLE();
  __HAL_RCC_PWR_CLK_ENABLE();
}

void SysTick_Handler(void)
{
  HAL_IncTick();
}

include $(PROJECT_PATH)/../../board/board.mk

ifeq ($(HAS_MODBUS), Y)
include $(PROJECT_PATH)/../../components/modbus/modbus.mk
endif

ifeq ($(HAS_BME680), Y)
include $(PROJECT_PATH)/../../components/bme680/bme680.mk
endif

ifeq ($(HAS_SWTIMER), Y)
include $(PROJECT_PATH)/../../components/swtimer/swtimer.mk
endif

ifeq ($(HAS_UIO), Y)
include $(PROJECT_PATH)/../../components/uio/uio.mk
endif

ifeq ($(HAS_TLC59116), Y)
include $(PROJECT_PATH)/../../components/tlc59116/tlc59116.mk
endif

######################################
# building variables
######################################
# optimization
ifeq ($(TARGET), Debug)
    OPT = -O0
else
    OPT = -O3
endif

#######################################
# binaries
#######################################
PREFIX = arm-none-eabi-
CC   = $(PREFIX)gcc
LD   = $(PREFIX)gcc
CP   = $(PREFIX)objcopy
AS   = $(PREFIX)gcc -x assembler-with-cpp
SZ   = $(PREFIX)size
HEX  = $(CP) -O ihex -R RAM --gap-fill 0xFF
BIN  = $(CP) -O binary -R RAM --gap-fill 0xFF

#######################################
# CFLAGS
#######################################
# cpu
CPU = -mcpu=cortex-m0

#fpu
FPU =

# float-abi
FLOAT-ABI = -mfloat-abi=soft

# mcu
MCU = $(CPU) -mthumb $(FPU) $(FLOAT-ABI) -fstack-usage -fno-common -std=gnu11

# AS defines
AS_DEFS = 

# C defines
C_DEFS += -DPROJECT=$(PROJECT)
C_DEFS += -D$(PROJECT)

# AS includes
AS_INCLUDES = 

# C includes
C_INCLUDES = $(patsubst %,-I%,$(C_INCLUDE))

# Warnings
C_WARN = -Werror -Werror=format-security -Wformat

# compile gcc flags
ASFLAGS = $(MCU) $(AS_DEFS) $(AS_INCLUDES) $(OPT) -Wall -fdata-sections -ffunction-sections -Wa,-amhls=$(LSTDIR)/$(notdir $(<:.s=.lst))
CFLAGS  = $(MCU) $(C_DEFS) $(OPT) $(C_WARN) -Wall -fdata-sections -ffunction-sections -Wa,-alms=$(LSTDIR)/$(notdir $(<:.c=.lst))

ifeq ($(TARGET), Debug)
    CFLAGS += -DDEBUG -g3
else
    CFLAGS += -DRELEASE -ggdb
endif

ASFLAGS  += -MMD -MP -MF $(DEPDIR)/$(@F).d
CFLAGS   += -MMD -MP -MF $(DEPDIR)/$(@F).d

#######################################
# LDFLAGS
#######################################
# link script
LDSCRIPT = $(PROJECT_PATH)\..\..\board\STM32F030K6TX_FLASH.ld

# libraries
LIBS    = -lc -lm -lnosys -static
LIBDIR  =
LDFLAGS = $(CPU) $(OPT) $(LIBS) -Wl,-Map=$(MAP),--cref,--no-warn-mismatch,--script=$(LDSCRIPT),--gc-sections --specs=nano.specs

#######################################
# paths
#######################################
BUILDDIR = build

# build outfiles
OUTFILES  = $(BUILDDIR)/$(BUILD_PREFIX)$(PROJECT).elf $(BUILDDIR)/$(BUILD_PREFIX)$(PROJECT).hex $(BUILDDIR)/$(BUILD_PREFIX)$(PROJECT).bin

# build directories
OBJDIR    = $(BUILDDIR)/$(BUILD_PREFIX)$(PROJECT)_obj
DEPDIR    = $(BUILDDIR)/$(BUILD_PREFIX)$(PROJECT)_dep
LSTDIR    = $(BUILDDIR)/$(BUILD_PREFIX)$(PROJECT)_lst
MAP       = $(BUILDDIR)/$(BUILD_PREFIX)$(PROJECT).map

# Object files groups
SRCPATHS  = $(sort $(dir $(ASM_SOURCES)) $(dir $(C_SOURCE)))
COBJS     = $(addprefix $(OBJDIR)/, $(notdir $(C_SOURCE:.c=.o)))
ASMOBJS   = $(addprefix $(OBJDIR)/, $(notdir $(ASM_SOURCES:.s=.o)))
OBJS	  = $(ASMOBJS) $(COBJS)

# Paths where to search for sources
VPATH     = $(SRCPATHS)

all: $(OBJDIR) $(LSTDIR) $(OBJS) $(OUTFILES)

#######################################
# build the application
#######################################
$(OBJDIR):
	mkdir -p $(OBJDIR)

$(LSTDIR):
	mkdir -p $(LSTDIR)

$(COBJS): $(OBJDIR)/%.o : %.c Makefile
	@echo
	$(CC) -c $(CFLAGS) -I. $(C_INCLUDES) $< -o $@
	
$(ASMOBJS) : $(OBJDIR)/%.o : %.s Makefile
	@echo
	$(AS) -c $(ASFLAGS) -I. $(C_INCLUDES) $< -o $@

%.elf: $(OBJS) $(LDSCRIPT)
	@echo
	$(LD) $(OBJS) $(LDFLAGS) -o $@

%.bin: %.elf
	$(BIN) $< $@
	
%.hex: %.elf
	$(HEX) $< $@

# Include the dependency files, should be the last of the makefile
-include $(shell mkdir -p $(DEPDIR) 2>/dev/null) $(wildcard $(DEPDIR)/*)
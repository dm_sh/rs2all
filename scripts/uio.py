import minimalmodbus
import serial.tools.list_ports
import time

mb                     = None
#modbus config
MODBUS_PORT            = 'COM14'
MODBUS_MODE            = 'rtu'
minimalmodbus.BAUDRATE = 9600
minimalmodbus.TIMEOUT  = 5
minimalmodbus.PARITY   = 'N'
slave_address          = 10
#modbus functions
MODBUS_READ_INPUT      = 0x4
MODBUS_WRITE_HOLDING   = 0x6
#modbus registers
REG_HOLDING_START           = 0x200
REG_HOLDING_CNFG_UIO_1      = 0x200
REG_HOLDING_CNFG_UIO_2      = 0x201
REG_HOLDING_CNFG_UIO_3      = 0x202
REG_HOLDING_CNFG_UIO_4      = 0x203
REG_HOLDING_CNFG_UIO_5      = 0x204
REG_HOLDING_CNFG_UIO_6      = 0x205
REG_HOLDING_CNFG_UIO_7      = 0x206
REG_HOLDING_CNFG_UIO_8      = 0x207
REG_HOLDING_SET_VALUE_UIO_1 = 0x208
REG_HOLDING_SET_VALUE_UIO_2 = 0x209
REG_HOLDING_SET_VALUE_UIO_3 = 0x20A
REG_HOLDING_SET_VALUE_UIO_4 = 0x20B
REG_HOLDING_SET_VALUE_UIO_5 = 0x20C
REG_HOLDING_SET_VALUE_UIO_6 = 0x20D
REG_HOLDING_SET_VALUE_UIO_7 = 0x20E
REG_HOLDING_SET_VALUE_UIO_8 = 0x20F

REG_INPUT_GET_VALUE_UIO_1   = 0x308
REG_INPUT_GET_VALUE_UIO_2   = 0x309
REG_INPUT_GET_VALUE_UIO_3   = 0x30A
REG_INPUT_GET_VALUE_UIO_4   = 0x30B
REG_INPUT_GET_VALUE_UIO_5   = 0x30C
REG_INPUT_GET_VALUE_UIO_6   = 0x30D
REG_INPUT_GET_VALUE_UIO_7   = 0x30E
REG_INPUT_GET_VALUE_UIO_8   = 0x30F
#uio mode
UIO_MODE_INPUT_RISING       = 1
UIO_MODE_INPUT_FALLING      = 2
UIO_MODE_OUTPUT             = 3
UIO_MODE_ADC                = 4
UIO_MODE_PWM                = 5
UIO_MODE_COUNTER            = 6

def connect():
  global mb
  mb = minimalmodbus.Instrument(MODBUS_PORT, slave_address, mode=MODBUS_MODE)
  if not mb.serial.is_open:
    try:
      mb.serial.open()
      print('{0} connection established'.format(MODBUS_PORT))
    except:
      print('{0} connection error'.format(MODBUS_PORT))

def board_config():
  global mb
  try:
    #config leds
    mb.write_register(REG_HOLDING_CNFG_UIO_1, UIO_MODE_OUTPUT, 0, MODBUS_WRITE_HOLDING)
    mb.write_register(REG_HOLDING_CNFG_UIO_4, UIO_MODE_OUTPUT, 0, MODBUS_WRITE_HOLDING)
    #config buttons
    mb.write_register(REG_HOLDING_CNFG_UIO_7, UIO_MODE_INPUT_RISING, 0, MODBUS_WRITE_HOLDING)
    mb.write_register(REG_HOLDING_CNFG_UIO_8, UIO_MODE_INPUT_RISING, 0, MODBUS_WRITE_HOLDING)
    #config adc
    mb.write_register(REG_HOLDING_CNFG_UIO_2, UIO_MODE_ADC, 0, MODBUS_WRITE_HOLDING)
    #time.sleep(0.1)
    print('Board is configured')
  except:
    print('MODBUS communication error')

def main():
  global mb
  connect()
  board_config()

  while (1):
    if mb.read_register(REG_INPUT_GET_VALUE_UIO_7, 1, MODBUS_READ_INPUT):
      mb.write_register(REG_HOLDING_SET_VALUE_UIO_1, 1, 0, MODBUS_WRITE_HOLDING)
    else:
      mb.write_register(REG_HOLDING_SET_VALUE_UIO_1, 0, 0, MODBUS_WRITE_HOLDING)

    if mb.read_register(REG_INPUT_GET_VALUE_UIO_8, 1, MODBUS_READ_INPUT):
      mb.write_register(REG_HOLDING_SET_VALUE_UIO_4, 1, 0, MODBUS_WRITE_HOLDING)
    else:
      mb.write_register(REG_HOLDING_SET_VALUE_UIO_4, 0, 0, MODBUS_WRITE_HOLDING)

    print (mb.read_register(REG_INPUT_GET_VALUE_UIO_2, 0, MODBUS_READ_INPUT))

    time.sleep(0.1)

if __name__ == '__main__':
  main()  

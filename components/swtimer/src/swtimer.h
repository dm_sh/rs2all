#ifndef SW_TIMER_MS_H_
#define SW_TIMER_MS_H_

#include <stdbool.h>
#include "stm32f0xx_hal.h"

typedef enum SwTimerId {
	SW_TIMER_ID_1,
	SW_TIMER_ID_2,
	SW_TIMER_ID_3,
	SW_TIMER_ID_4,
	SW_TIMER_ID_5,
	SW_TIMER_ID_NONE
} SwTimerId_t;

typedef enum SwTimerMsType {
	SW_TIMER_TYPE_CYCLE,
	SW_TIMER_TYPE_ONE_SHOT
} SwTimerMsType_t;

typedef struct SwTimerMsConfig {
	SwTimerMsType_t type;
	uint32_t        period_ms;
} SwTimerMsConfig_t;

typedef struct SwTimerMsState {
	SwTimerMsType_t type;
	uint32_t      period_ms;
	uint32_t      saved_ms;
	bool          is_active;
} SwTimerMsState_t;

SwTimerId_t swtimer_add_timer(SwTimerMsConfig_t timer_config);
bool swtimer_is_timeout(SwTimerId_t timer_id);
bool swtimer_start(SwTimerId_t timer_id);
bool swtimer_stop(SwTimerId_t timer_id);

#endif

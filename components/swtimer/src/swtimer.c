#include "swtimer.h"

#define SW_TIMER_MS_MAX_COUNT      5

static SwTimerMsState_t sw_timers[SW_TIMER_MS_MAX_COUNT];

static uint8_t sw_timer_ms_count = 0;

SwTimerId_t swtimer_add_timer(SwTimerMsConfig_t config) {
	if (sw_timer_ms_count == SW_TIMER_MS_MAX_COUNT) {
		return SW_TIMER_ID_NONE;
	}

	uint8_t timer_id = sw_timer_ms_count++;

	sw_timers[timer_id].period_ms = config.period_ms;
	sw_timers[timer_id].type      = config.type;
	sw_timers[timer_id].saved_ms  = HAL_GetTick();
	sw_timers[timer_id].is_active = (config.type == SW_TIMER_TYPE_CYCLE) ? true : false;

	return (SwTimerId_t)timer_id;
}

bool swtimer_is_timeout(SwTimerId_t timer_id) {
	uint32_t time_passed = HAL_GetTick() - sw_timers[timer_id].saved_ms;
	bool     is_timeout  = (time_passed >= sw_timers[timer_id].period_ms) ? true : false;

	if (sw_timers[timer_id].is_active && is_timeout) {
		if (sw_timers[timer_id].type == SW_TIMER_TYPE_CYCLE) {
			sw_timers[timer_id].saved_ms = HAL_GetTick();
		}

		return true;
	}

	return false;
}

bool swtimer_start(SwTimerId_t timer_id) {
	if (sw_timers[timer_id].type == SW_TIMER_TYPE_CYCLE) {
		return false;
	}

	sw_timers[timer_id].saved_ms = HAL_GetTick();
	sw_timers[timer_id].is_active = true;
	return true;
}

bool swtimer_stop(SwTimerId_t timer_id) {
	if (sw_timers[timer_id].type == SW_TIMER_TYPE_CYCLE) {
		return false;
	}

	sw_timers[timer_id].is_active = false;
	return true;
}

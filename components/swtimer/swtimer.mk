SWTIMER_PATH := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))

C_DEFS += -DHAS_SWTIMER

C_INCLUDE += $(SWTIMER_PATH)/src

C_SOURCE += $(SWTIMER_PATH)/src/swtimer.c

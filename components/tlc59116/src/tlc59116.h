#ifndef TLC59116_H_
#define TLC59116_H_

#include <stdbool.h>
#include <stdint.h>

// #define TLC_REG_MODE1                 0x00 //0x80
// #define TLC_REG_MODE2                 0x01 //0x80
// #define TLC_REG_PWM0                  0x02 //8 gradations
// #define TLC_REG_PWM1                  0x03 //8 gradations
// #define TLC_REG_PWM2                  0x04 //8 gradations
// #define TLC_REG_PWM3                  0x05 //8 gradations
// #define TLC_REG_PWM4                  0x06 //8 gradations
// #define TLC_REG_PWM5                  0x07 //8 gradations
// #define TLC_REG_PWM6                  0x08 //8 gradations
// #define TLC_REG_PWM7                  0x09 //8 gradations
// #define TLC_REG_PWM8                  0x0A //8 gradations
// #define TLC_REG_PWM9                  0x0B //8 gradations
// #define TLC_REG_PWM10                 0x0C //8 gradations
// #define TLC_REG_PWM11                 0x0D //8 gradations
// #define TLC_REG_PWM12                 0x0E //8 gradations
// #define TLC_REG_PWM13                 0x0F //8 gradations
// #define TLC_REG_PWM14                 0x10 //8 gradations
// #define TLC_REG_PWM15                 0x11 //8 gradations
// #define TLC_REG_GRPPWM                0x12 //0
// #define TLC_REG_GRPFREQ               0x13 //0
// #define TLC_REG_LEDOUT0               0x14 //0xAA
// #define TLC_REG_LEDOUT1               0x15 //0xAA
// #define TLC_REG_LEDOUT2               0x16 //0xAA
// #define TLC_REG_LEDOUT3               0x17 //0xAA
// #define TLC_REG_SUBADR1               0x18 //0
// #define TLC_REG_SUBADR2               0x19 //0
// #define TLC_REG_SUBADR3               0x1A //0
// #define TLC_REG_ALLCALLADR            0x1B //0
// #define TLC_REG_IREF                  0x1C //0xFF
// #define TLC_REG_EFLAG1                0x1D //0 
// #define TLC_REG_EFLAG2                0x1E //0

#define TLC_GLOBAL_CMD_ALL_SIZE       0x1F
#define TLC_GLOBAL_CMD_ALL            0x80
#define TLC_GLOBAL_CMD_PWM_SIZE       0x10   
#define TLC_GLOBAL_CMD_PWM            0xA2        

typedef enum TLCBrightness {
  TLC_BRIGHT_1,
  TLC_BRIGHT_2,
  TLC_BRIGHT_3,
  TLC_BRIGHT_4,
  TLC_BRIGHT_5,
  TLC_BRIGHT_6,
  TLC_BRIGHT_7,
  TLC_BRIGHT_8,
  TLC_BRIGHT_DISABLE,
  TLC_BRIGHT_COUNT
} TLCBrightness_t;

typedef enum TLCChannel {
  TLC_CH_0    = 0x02,
  TLC_CH_1    = 0x03,
  TLC_CH_2    = 0x04,
  TLC_CH_3    = 0x05,
  TLC_CH_4    = 0x06,
  TLC_CH_5    = 0x07,
  TLC_CH_6    = 0x08,
  TLC_CH_7    = 0x09,
  TLC_CH_8    = 0x0A,
  TLC_CH_9    = 0x0B,
  TLC_CH_10   = 0x0C,
  TLC_CH_11   = 0x0D,
  TLC_CH_12   = 0x0E,
  TLC_CH_13   = 0x0F,
  TLC_CH_14   = 0x10,
  TLC_CH_15   = 0x11,
} TLCChannel_t;

typedef bool (*i2c_read_func)(uint8_t, uint8_t, uint8_t*, uint16_t);
typedef bool (*i2c_write_func)(uint8_t, uint8_t, uint8_t*, uint16_t);

void tlc_init(i2c_read_func read_func, i2c_write_func write_func);
bool tlc_setup(uint8_t address);
bool tlc_enable_chs_all(uint8_t address, TLCBrightness_t bright);
bool tlc_disable_chs_all(uint8_t address);
bool tlc_enable_ch(uint8_t address, TLCBrightness_t bright, TLCChannel_t channel);
bool tlc_disable_ch(uint8_t address, TLCChannel_t channel);
bool tlc_enable_chs(uint8_t address, TLCBrightness_t bright, const bool *channels);

#endif

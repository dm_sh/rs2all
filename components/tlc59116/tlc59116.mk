TLC59116_PATH := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))

C_DEFS += -DHAS_TLC59116

C_INCLUDE += $(TLC59116_PATH)/src

C_SOURCE += $(TLC59116_PATH)/src/tlc59116.c

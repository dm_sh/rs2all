#include "uio.h"

#define UIO_COUNT						        8

static UIOSettings_t uios[UIO_COUNT] = {
	//name    mode              port   pin         timer  adc   adc_channel   wr rd
	{ UIO_1, UIO_MODE_UNKNOWN, GPIOB, GPIO_PIN_1, true,  true, ADC_CHANNEL_9, 0, 0 },
	{ UIO_2, UIO_MODE_UNKNOWN, GPIOB, GPIO_PIN_0, true,  true, ADC_CHANNEL_8, 0, 0 },
	{ UIO_3, UIO_MODE_UNKNOWN, GPIOA, GPIO_PIN_7, true,  true, ADC_CHANNEL_7, 0, 0 },
	{ UIO_4, UIO_MODE_UNKNOWN, GPIOA, GPIO_PIN_6, true,  true, ADC_CHANNEL_6, 0, 0 },
	{ UIO_5, UIO_MODE_UNKNOWN, GPIOA, GPIO_PIN_8, true,  false, 0 },
	{ UIO_6, UIO_MODE_UNKNOWN, GPIOB, GPIO_PIN_3, false, false, 0 },
	{ UIO_7, UIO_MODE_UNKNOWN, GPIOB, GPIO_PIN_4, true,  false, 0 },
	{ UIO_8, UIO_MODE_UNKNOWN, GPIOB, GPIO_PIN_5, true,  false, 0 }
};

static ADC_HandleTypeDef hadc1;

static void _uio_set_mode(uint8_t uio_index, UIOMode_t mode);
static void _uio_set_mode_input_rising(uint8_t uio_index);
static void _uio_set_mode_input_falling(uint8_t uio_index);
static void _uio_enable_exti_irq(uint16_t gpio);
static void _uio_set_mode_output(uint8_t uio_index);
static void _uio_set_mode_adc(uint8_t uio_index);
static void _uio_set_mode_pwm(uint8_t uio_index);

bool uio_init() {
	hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
	hadc1.Init.LowPowerAutoPowerOff = DISABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.SamplingTimeCommon = ADC_SAMPLETIME_41CYCLES_5;

  if (HAL_ADC_Init(&hadc1) != HAL_OK) {
    return false;
  }
	//TODO: restore state from flash
	return true;
}

void uio_configure(UIOName_t uio, uint16_t config) {
	UIOMode_t mode = (UIOMode_t)(config & 0xFF);
	if (uios[uio].mode != mode) {
		_uio_set_mode(uio, mode);
	}
}

uint16_t uio_get_readout(UIOName_t uio) {
	uint16_t value = 0;

	if ((uios[uio].mode == UIO_MODE_INPUT_RISING) || (uios[uio].mode == UIO_MODE_INPUT_FALLING)) {
		value = uios[uio].rd_value;
	}
	
	if (uios[uio].mode == UIO_MODE_ADC) {
		HAL_ADC_Start(&hadc1);
  	HAL_ADC_PollForConversion(&hadc1, 50);
		value = HAL_ADC_GetValue(&hadc1);
		HAL_ADC_Stop(&hadc1);
		value = (uint16_t)RAW_ADC_VALUE_TO_mV(value);
	}

	return value;
}

void uio_set_value(UIOName_t uio, uint16_t value) {
	if (uios[uio].mode != UIO_MODE_OUTPUT) {
		return;
	}

	if (uios[uio].wr_value != value) {
		uios[uio].wr_value = value;

		if (value >= 1) {
			HAL_GPIO_WritePin(uios[uio].port, uios[uio].pin, SET);
		} else {
			HAL_GPIO_WritePin(uios[uio].port, uios[uio].pin, RESET);
		}
	}
}

static void _uio_set_mode(UIOName_t uio, UIOMode_t mode) {
	switch (mode) {
		case UIO_MODE_INPUT_RISING: _uio_set_mode_input_rising(uio); break;
		case UIO_MODE_INPUT_FALLING: _uio_set_mode_input_falling(uio); break;
		case UIO_MODE_OUTPUT: _uio_set_mode_output(uio); break;
		case UIO_MODE_ADC: _uio_set_mode_adc(uio); break;
		case UIO_MODE_PWM: _uio_set_mode_pwm(uio); break;
		default: break;
	}
}

static void _uio_set_mode_input_rising(UIOName_t uio) {
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	if (uios[uio].mode != UIO_MODE_UNKNOWN) {
		return;
	}

	uios[uio].mode = UIO_MODE_INPUT_RISING;

	GPIO_InitStruct.Pin = uios[uio].pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(uios[uio].port, &GPIO_InitStruct);

	_uio_enable_exti_irq(uios[uio].pin);
}

static void _uio_set_mode_input_falling(UIOName_t uio) {
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	if (uios[uio].mode != UIO_MODE_UNKNOWN) {
		return;
	}

	uios[uio].mode = UIO_MODE_INPUT_FALLING;

	GPIO_InitStruct.Pin = uios[uio].pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(uios[uio].port, &GPIO_InitStruct);

	_uio_enable_exti_irq(uios[uio].pin);
}

static void _uio_enable_exti_irq(uint16_t gpio) {
	if ((gpio == GPIO_PIN_0) || (gpio == GPIO_PIN_1)) {
		HAL_NVIC_SetPriority(EXTI0_1_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);
	} else if ((gpio == GPIO_PIN_2) || (gpio == GPIO_PIN_3)) {
		HAL_NVIC_SetPriority(EXTI2_3_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(EXTI2_3_IRQn);
	} else {
		HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
	}
}

static void _uio_set_mode_output(UIOName_t uio) {
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	if (uios[uio].mode != UIO_MODE_UNKNOWN) {
		return;
	}

	uios[uio].mode = UIO_MODE_OUTPUT;

	GPIO_InitStruct.Pin   = uios[uio].pin;
	GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
	HAL_GPIO_Init(uios[uio].port, &GPIO_InitStruct);
}

static void _uio_set_mode_adc(UIOName_t uio) {
	if (!uios[uio].has_adc) {
		return;
	}

	GPIO_InitTypeDef GPIO_InitStruct = {0};

	GPIO_InitStruct.Pin       = uios[uio].pin;
  GPIO_InitStruct.Mode      = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;

	HAL_GPIO_Init(uios[uio].port, &GPIO_InitStruct);

  ADC_ChannelConfTypeDef sConfig = {0};

  sConfig.Channel      = uios[uio].adc_channel;
  sConfig.Rank         = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_41CYCLES_5;

  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
    return;
  }

  if (HAL_ADCEx_Calibration_Start(&hadc1) != HAL_OK) {
    return;
  }

	uios[uio].mode = UIO_MODE_ADC;
}

static void _uio_set_mode_pwm(UIOName_t uio) {

}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	for (int i = 0; i < UIO_COUNT; i++) {
		if (uios[i].pin == GPIO_Pin) {
			uios[i].rd_value = (uint8_t)HAL_GPIO_ReadPin(uios[i].port, uios[i].pin);
		}
	}
}

void EXTI0_1_IRQHandler(void) {
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}

void EXTI2_3_IRQHandler(void) {
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}

void EXTI4_15_IRQHandler(void) {
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_5);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_6);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_7);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_8);
}

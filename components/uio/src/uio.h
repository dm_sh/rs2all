#ifndef UIO_H_
#define UIO_H_

#include <stdbool.h>
#include "stm32f0xx_hal.h"

#define RAW_ADC_TO_mV_COEFF                  (float)((3300.0f) / (0xFFF))
#define RAW_ADC_VALUE_TO_mV(raw_adc)         (float)((raw_adc) * (RAW_ADC_TO_mV_COEFF))

typedef enum UIOName {
	UIO_1 = 0,
	UIO_2,
	UIO_3,
	UIO_4,
	UIO_5,
	UIO_6,
	UIO_7,
	UIO_8,
} UIOName_t;

typedef enum UIOCommand {
	UIO_CMD_UNKNOWN = 0x00,
	UIO_CMD_SET_MODE = 0x01,
	UIO_CMD_READ_VALUE = 0x02,
	UIO_CMD_WRITE_VALUE = 0x04,
} UIOCommand_t;

typedef enum UIOMode {
	UIO_MODE_UNKNOWN = 0,
	UIO_MODE_INPUT_RISING = 1,
	UIO_MODE_INPUT_FALLING = 2,
	UIO_MODE_OUTPUT = 3,
	UIO_MODE_ADC = 4,
	UIO_MODE_PWM = 5,
	UIO_MODE_COUNTER = 6,
} UIOMode_t;

typedef struct UIOSettings {
	UIOName_t name;
	UIOMode_t mode;
	GPIO_TypeDef* port;
	uint16_t pin;
	bool has_timer;
	bool has_adc;
	uint32_t adc_channel;
	uint16_t wr_value;
	uint16_t rd_value;
} UIOSettings_t;

bool uio_init();
void uio_configure(UIOName_t uio, uint16_t config);
void uio_set_value(UIOName_t uio, uint16_t value);
uint16_t uio_get_readout(UIOName_t uio);

#endif

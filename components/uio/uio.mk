UIO_PATH := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))

C_DEFS += -DHAS_UIO

C_INCLUDE += $(UIO_PATH)/src

C_SOURCE += $(UIO_PATH)/src/uio.c

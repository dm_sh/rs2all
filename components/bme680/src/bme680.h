#ifndef SENSORS_H_
#define SENSORS_H_

#include <stdbool.h>
#include "stm32f0xx_hal.h"
#include "bme680_driver.h"

typedef struct SensorsData {
	/*! Temperature in degree celsius x100 */
	int16_t temperature;
	/*! Pressure in Pascal */
	uint32_t pressure;
	/*! Humidity in % relative humidity x1000 */
	uint32_t humidity;
	/*! Gas resistance in Ohms */
	uint32_t gas_resistance;
} SensorsData_t;

typedef bool (*i2c_read_func)(uint8_t, uint8_t, uint8_t*, uint16_t);
typedef bool (*i2c_write_func)(uint8_t, uint8_t, uint8_t*, uint16_t);

bool sensors_init(i2c_read_func read_func, i2c_write_func write_func);
SensorsData_t sensors_get_data();

#endif

#include "bme680.h"
#include <string.h>

static struct bme680_dev           bme680_sensor;

static SensorsData_t sensors_data   = {0,0,0,0};
static bool SENSORS_BME680_present  = false;
static uint16_t sensors_bme680_meas = 0;

static bool   _sensors_bme680_init();

bool sensors_init() {
	SENSORS_BME680_present = _sensors_bme680_init();
	bme680_get_profile_dur(&sensors_bme680_meas, &bme680_sensor);

	return true;
}

SensorsData_t sensors_get_data()
{
	struct bme680_field_data bme680_data;

	bme680_sensor.power_mode = BME680_FORCED_MODE;

	if (bme680_set_sensor_mode(&bme680_sensor) == BME680_OK) {
		HAL_Delay(sensors_bme680_meas);

		if (bme680_get_sensor_data(&bme680_data, &bme680_sensor) == BME680_OK) {
			sensors_data.temperature = bme680_data.temperature;
			sensors_data.humidity = bme680_data.humidity;
			sensors_data.pressure = bme680_data.pressure;

			if(bme680_data.status & BME680_GASM_VALID_MSK) {
				sensors_data.gas_resistance = bme680_data.gas_resistance;
			}
		}
	}

	bme680_sensor.power_mode = BME680_SLEEP_MODE;
	bme680_set_sensor_mode(&bme680_sensor);

	return sensors_data;
}

static bool _sensors_bme680_init() {
	bme680_sensor.dev_id              = BME680_I2C_ADDR_PRIMARY;
	bme680_sensor.intf                = BME680_I2C_INTF;
	bme680_sensor.write 		          = _sensors_i2c_write;
	bme680_sensor.read                = _sensors_i2c_read;
	bme680_sensor.delay_ms            = HAL_Delay;
	bme680_sensor.amb_temp            = 25;

	if (bme680_init(&bme680_sensor) != BME680_OK) {
		return false;
	}

	/* Set the temperature, pressure and humidity settings */
	bme680_sensor.tph_sett.os_hum     = BME680_OS_2X;
	bme680_sensor.tph_sett.os_pres    = BME680_OS_2X;
	bme680_sensor.tph_sett.os_temp    = BME680_OS_2X;
	bme680_sensor.tph_sett.filter     = BME680_FILTER_SIZE_3;
	/* Set the remaining gas sensor settings and link the heating profile */
	bme680_sensor.gas_sett.run_gas    = BME680_DISABLE_GAS_MEAS;
	bme680_sensor.gas_sett.heatr_temp = 0; /* degree Celsius */
	bme680_sensor.gas_sett.heatr_dur  = 0; /* milliseconds */
	/* Select the power mode */
	bme680_sensor.power_mode          = BME680_SLEEP_MODE;
	/* Set the required sensor settings needed */
	uint8_t settings = BME680_OST_SEL | BME680_OSP_SEL | BME680_OSH_SEL | BME680_FILTER_SEL;

	/* Set the desired sensor configuration */
	if (bme680_set_sensor_settings(settings, &bme680_sensor) != BME680_OK) {
		return false;
	}
	/* Set the power mode */
	if (bme680_set_sensor_mode(&bme680_sensor) != BME680_OK) {
		return false;
	}

	return true;
}

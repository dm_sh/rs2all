BME680_PATH := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))

C_DEFS += -DHAS_BME680

C_INCLUDE += $(BME680_PATH)/src

C_SOURCE += $(BME680_PATH)/src/bme680_driver.c
C_SOURCE += $(BME680_PATH)/src/bme680.c

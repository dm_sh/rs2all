#ifndef MODBUS_H_
#define MODBUS_H_

#include <stdbool.h>
#include "stm32f0xx_hal.h"
#include "stm32f0xx_it.h"

bool modbus_init();
void modbus_poll();

#endif

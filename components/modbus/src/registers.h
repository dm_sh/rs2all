#ifndef REGISTERS_H_
#define REGISTERS_H_

#define REG_COIL_START				        0x000
#define REG_COIL_NREGS				        0

#define REG_DISCRETE_START			      0x100
#define REG_DISCRETE_NREGS			      0

#define REG_HOLDING_START			        0x200
#define	REG_HOLDING_CNFG_UIO_1        0
#define	REG_HOLDING_CNFG_UIO_2        1
#define	REG_HOLDING_CNFG_UIO_3        2
#define	REG_HOLDING_CNFG_UIO_4        3
#define	REG_HOLDING_CNFG_UIO_5        4
#define	REG_HOLDING_CNFG_UIO_6        5
#define	REG_HOLDING_CNFG_UIO_7        6
#define	REG_HOLDING_CNFG_UIO_8        7
#define REG_HOLDING_SET_VALUE_OFFSET  8
#define	REG_HOLDING_SET_VALUE_UIO_1   8
#define	REG_HOLDING_SET_VALUE_UIO_2   9
#define	REG_HOLDING_SET_VALUE_UIO_3   10
#define	REG_HOLDING_SET_VALUE_UIO_4   11
#define	REG_HOLDING_SET_VALUE_UIO_5   12
#define	REG_HOLDING_SET_VALUE_UIO_6   13
#define	REG_HOLDING_SET_VALUE_UIO_7   14
#define	REG_HOLDING_SET_VALUE_UIO_8   15
#define REG_HOLDING_NREGS			        16

#define REG_INPUT_START				        0x300
#define REG_INPUT_TEMP                0
#define REG_INPUT_HUMI                2
#define REG_INPUT_PRES                4
#define REG_INPUT_GAS                 6
#define REG_INPUT_GET_VaLUE_OFFSET    8
#define REG_INPUT_GET_VALUE_UIO_1     8
#define REG_INPUT_GET_VALUE_UIO_2     9
#define REG_INPUT_GET_VALUE_UIO_3     10
#define REG_INPUT_GET_VALUE_UIO_4     11
#define REG_INPUT_GET_VALUE_UIO_5     12
#define REG_INPUT_GET_VALUE_UIO_6     13
#define REG_INPUT_GET_VALUE_UIO_7     14
#define REG_INPUT_GET_VALUE_UIO_8     15
#define REG_INPUT_NREGS				        16

#ifdef HAS_BME680
void registers_set_temperature(int32_t temp);
void registers_set_humidity(uint32_t humi);
void registers_set_pressure(uint32_t pres);
void registers_set_gas(uint32_t gas);
#endif

#ifdef HAS_UIO
typedef enum RegUIO {
  REG_UIO_1 = 0,
  REG_UIO_2,
  REG_UIO_3,
  REG_UIO_4,
  REG_UIO_5,
  REG_UIO_6,
  REG_UIO_7,
  REG_UIO_8,
} RegUIO_t;

uint16_t registers_get_config_uio(RegUIO_t uio);
uint16_t registers_get_wr_value_uio(RegUIO_t uio);
void     registers_set_rd_value_uio(RegUIO_t uio, uint16_t value);
#endif

#endif

MODBUS_PATH := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))

C_DEFS += -DHAS_MODBUS

C_INCLUDE += $(MODBUS_PATH)/drv/include
C_INCLUDE += $(MODBUS_PATH)/drv/rtu
C_INCLUDE += $(MODBUS_PATH)/port
C_INCLUDE += $(MODBUS_PATH)/src

C_SOURCE += $(MODBUS_PATH)/drv/functions/mbfunccoils.c
C_SOURCE += $(MODBUS_PATH)/drv/functions/mbfuncdiag.c
C_SOURCE += $(MODBUS_PATH)/drv/functions/mbfuncdisc.c
C_SOURCE += $(MODBUS_PATH)/drv/functions/mbfuncholding.c
C_SOURCE += $(MODBUS_PATH)/drv/functions/mbfuncinput.c
C_SOURCE += $(MODBUS_PATH)/drv/functions/mbfuncother.c
C_SOURCE += $(MODBUS_PATH)/drv/functions/mbutils.c
C_SOURCE += $(MODBUS_PATH)/drv/rtu/mbcrc.c
C_SOURCE += $(MODBUS_PATH)/drv/rtu/mbrtu.c
C_SOURCE += $(MODBUS_PATH)/drv/mb.c
C_SOURCE += $(MODBUS_PATH)/port/portevent.c
C_SOURCE += $(MODBUS_PATH)/port/portserial.c
C_SOURCE += $(MODBUS_PATH)/port/porttimer.c
C_SOURCE += $(MODBUS_PATH)/src/registers.c
C_SOURCE += $(MODBUS_PATH)/src/modbus.c

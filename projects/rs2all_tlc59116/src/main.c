#include "main.h"
#include "clock.h"
#include "swtimer.h"
#include "led.h"
#include "tlc59116.h"
#include "i2c.h"

int main(void)
{
  HAL_Init();

  if (!clock_init()) {
	  while (1) { }
  }

  led_init();

  i2c_init();

  tlc_init(i2c_read, i2c_write);
  tlc_setup(0x60);

  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_0);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_1);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_2);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_3);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_4);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_5);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_6);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_7);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_8);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_9);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_10);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_11);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_12);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_13);
  tlc_enable_ch(0x60, TLC_BRIGHT_8, TLC_CH_14);

  SwTimerMsConfig_t config;
  config.type = SW_TIMER_TYPE_CYCLE;
  config.period_ms = 500;
  SwTimerId_t led_timer = swtimer_add_timer(config);

  while (1)
  {
	  if (swtimer_is_timeout(led_timer)) {
		  led_toggle();
	  }
  }
}

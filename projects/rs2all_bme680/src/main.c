#include "main.h"
#include "clock.h"
#include "swtimer.h"
#include "led.h"
#include "modbus.h"
#include "bme680.h"
#include "registers.h"

int main(void)
{
  HAL_Init();

  if (!clock_init()) {
	  while (1) { }
  }

  led_init();

  if (!modbus_init()) {
	  return false;
  }

  if (!sensors_init()) {
	  return false;
  }

  SensorsData_t sensor_data;

  SwTimerMsConfig_t config;
  config.type = SW_TIMER_TYPE_CYCLE;
  config.period_ms = 500;
  SwTimerId_t led_timer = swtimer_add_timer(config);

  config.period_ms = 5000;
  SwTimerId_t data_timer = swtimer_add_timer(config);

  while (1)
  {
	  modbus_poll();

	  if (swtimer_is_timeout(led_timer)) {
		  led_toggle();
	  }

    if (swtimer_is_timeout(data_timer)) {
		  sensor_data = sensors_get_data();

		  registers_set_temperature(sensor_data.temperature);
		  registers_set_humidity(sensor_data.humidity);
		  registers_set_pressure(sensor_data.pressure);
		  registers_set_gas(sensor_data.gas_resistance);
	  }
  }
}

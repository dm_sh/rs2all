#include "main.h"
#include "clock.h"
#include "swtimer.h"
#include "led.h"
#include "modbus.h"
#include "registers.h"
#include "uio.h"

static void update_uio_config();
static void update_uio_values();
static void update_input_registers();

int main(void)
{
  HAL_Init();

  if (!clock_init()) {
	  while (1) { }
  }

  led_init();

  if (!modbus_init()) {
	  return false;
  }

  if (!uio_init()) {
	  return false;
  }

  SwTimerMsConfig_t config;
  config.type = SW_TIMER_TYPE_CYCLE;
  config.period_ms = 500;
  SwTimerId_t led_timer = swtimer_add_timer(config);

  while (1)
  {
	  modbus_poll();

    update_uio_config();
    update_uio_values();
    update_input_registers();
    
	  if (swtimer_is_timeout(led_timer)) {
		  led_toggle();
	  }
  }
}

static void update_uio_config() {
  uio_configure(UIO_1, registers_get_config_uio(REG_UIO_1));
  uio_configure(UIO_2, registers_get_config_uio(REG_UIO_2));
  uio_configure(UIO_3, registers_get_config_uio(REG_UIO_3));
  uio_configure(UIO_4, registers_get_config_uio(REG_UIO_4));
  uio_configure(UIO_5, registers_get_config_uio(REG_UIO_5));
  uio_configure(UIO_6, registers_get_config_uio(REG_UIO_6));
  uio_configure(UIO_7, registers_get_config_uio(REG_UIO_7));
  uio_configure(UIO_8, registers_get_config_uio(REG_UIO_8));
}

static void update_uio_values() {
  uio_set_value(UIO_1, registers_get_wr_value_uio(REG_UIO_1));
  uio_set_value(UIO_2, registers_get_wr_value_uio(REG_UIO_2));
  uio_set_value(UIO_3, registers_get_wr_value_uio(REG_UIO_3));
  uio_set_value(UIO_4, registers_get_wr_value_uio(REG_UIO_4));
  uio_set_value(UIO_5, registers_get_wr_value_uio(REG_UIO_5));
  uio_set_value(UIO_6, registers_get_wr_value_uio(REG_UIO_6));
  uio_set_value(UIO_7, registers_get_wr_value_uio(REG_UIO_7));
  uio_set_value(UIO_8, registers_get_wr_value_uio(REG_UIO_8));
}

static void update_input_registers() {
  registers_set_rd_value_uio(REG_UIO_1, uio_get_readout(UIO_1));
  registers_set_rd_value_uio(REG_UIO_2, uio_get_readout(UIO_2));
  registers_set_rd_value_uio(REG_UIO_3, uio_get_readout(UIO_3));
  registers_set_rd_value_uio(REG_UIO_4, uio_get_readout(UIO_4));
  registers_set_rd_value_uio(REG_UIO_5, uio_get_readout(UIO_5));
  registers_set_rd_value_uio(REG_UIO_6, uio_get_readout(UIO_6));
  registers_set_rd_value_uio(REG_UIO_7, uio_get_readout(UIO_7));
  registers_set_rd_value_uio(REG_UIO_8, uio_get_readout(UIO_8));
}